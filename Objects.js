// Objects are dynamic in nature and referenced datatype Due to which we can redeclare them

// const course={
//     lecture:10,
//     section:3,
//     title:'Javascript',
//     notes:{
//         introduction:"Welcome"
//     },
//     enroll(){
//         console.log("You are successfully enrolled");
//     }
// }

// course.enroll();
// console.log(course);
// course.price=150;
// console.log(course);

// Creating function
// course.checkenrollment=function()
// {
//     console.log(`30 users are enrolled`)
// }


// Factory function / Generic functions 

function createCourse()
{
return{
    lecture:10,
    section:3,
    title:'Javascript',
    notes:{
        introduction:"Welcome"
    },
    enroll(){
        console.log("You are successfully enrolled");
    }
};


}

// const course=createCourse();
// course.enroll();



// Constructor Function 
function Course(title){
    this.title=title;
    this.enroll=function()
    {
        console.log("You are successfully enrolled");
    }
}

//## new will create empty object and it will return this keyword
const course=new Course('Javascript')
// course.enroll();

// We can also delete the course elements as it is created dynamically 

delete course.title;
course.checkenrollment=function()
{
    console.log(`30 users are enrolled`)
}
console.log(course);
